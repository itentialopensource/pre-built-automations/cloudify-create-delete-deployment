<!-- This is a comment in md (Markdown. format, it will not be visible to the end user -->

<!-- Update the below line with your Pre-Built name -->
# Cloudify Create Delete Deployment

<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents

* [Overview](#overview)
* [Main Workflow](#main-workflow)
* [Requirements](#requirements)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)
* [Additional Information](#additional-information)

## Overview

The **Cloudify Create Delete Deployment** pre-built consists of workflows to create and delete deployments on Cloudify Manager.
This pre-built was designed and tested using a Cloudify Manager instance downloaded as a Docker image with the created container running on an Itential DSUP server. Consequently, this pre-built should create and delete deployments on any Cloudify instance with little to no modification. The service configuration of the corresponding adapter instance must be verified on IAP if Cloudify Manager is served on a different host.

_Estimated Run Time_: 3 minutes

## Main Workflows

Pictured below are the main workflows for the creation and deletion of deployments on Cloudify Manager.

**Create Deployment**

<table><tr><td>
  <img src="./images/create_deployment.png" alt="Create Deployment" width="600px">
</td></tr></table>

Below is an explanation for each of the numbered tasks in the create deployment workflow, respectively:

1. Query form data from inputs.
2. Query form data for deployment id.
3. Set-up the input object from the form data to be provided to the tasks to perform a create deployment operation.
4. Set-up data by converting inputs from form data to JSON.
5. Set an object key to construct a deployment body for Cloudify.
6. Create deployment task for Cloudify which creates the deployment in Cloudify Manager.
7. Make data payload for the Cloudify start execution task.
8. A delay of 30 seconds for Cloudify to get ready.
9. Query response from the error object from the createDeployment task.
10. Execute a workflow on a given deployment.
11. Retry execution for a specific number of times if start execution errors out.
12. Query execution id to be used in further tasks.
13. Show response from start execution failure and an option to retry start execution task.
14. Retrieve information for a specific execution.
15. View error response from the create deployment task.
16. Run an evaluation to check status for failure.
17. Run an evaluation to check status for terminated.
18. Retry the status check or get execution task.
19. The status check or get execution task has not passed for the deployment.
20. Get deployments outputs from Cloudify.

**Delete Deployment**

<table><tr><td>
  <img src="./images/delete_deployment.png" alt="Delete Deployment" width="600px">
</td></tr></table>

Below is an explanation for each of the numbered tasks in the delete deployment workflow, respectively:

1. Query deployment id from form data to be used in the workflow.
2. Make data payload for the Cloudify delete execution task.
3. Execute a workflow on a given deployment.
4. A delay of 60 seconds for Cloudify to get ready.
5. Query response from the error object from the delete execution task.
6. Delete Cloudify deployment task which deletes the deployment in Cloudify Manager.
7. Retry delete deployment task if it errors out.
8. Query response from error object from the delete deployment.
9. View response from Cloudify delete deployment failure.
10. View response from error object from the delete execution task.


## Requirements
Pre-requisites to use the Cloudify Create Delete Deployment include:

- Itential Automation Platform
  - ^2021.2

- Cloudify Manager
  - A Cloudify Manager instance running on a local machine or dedicated server.
  - Service configuration to setup an adapter in IAP to connect to Cloudify Manager.
  - A blueprint must be available on Cloudify Manager to be used in the form to create and delete deployments.


## How to Install
To install the Pre-Built:

* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Requirements](#requirements) section in order to install the Pre-Built.
* The Pre-Built can be installed from within Pre-builts catalog. Simply search for the name of your desired Pre-Built and click the install button (as shown below).


<table><tr><td>
  <img src="./images/install.png" alt="install" width="600px">
</td></tr></table>

* After the notification displays to inform you the Pre-built has been installed, you should be able to navigate to **Operations Mangager** and verify the **Cloudify Create Delete Deployment** item has been successfully installed.
* Alternatively, you may clone this repository and run `npm pack` to create a tarball which can then be installed via the offline installer in **App-Artifacts**. Please consult the **App-Artifacts** documentation for further information.

## How to Run

Use of this Pre-Built assumes there is a Cloudify Manager instance running and available to perform create and delete deployment operations.

* Once Cloudify Manager is up and running, an adapter instance of the same can be added from the **Settings→Services→Adapters** section in IAP.
* Once the adapter instance is added and the adapter status and connection shows as green (active) under the **Settings→System→Adapters** menu in IAP, the create/delete deployment job can be started from Operations Manager. 
* A unique deployment id which starts with a letter must be given, and the name of the blueprint previously uploaded to Cloudify Manager must also be provided. 
* If no further input options are needed, an empty object {} must be provided in the inputs field. To explore further input options for Cloudify deployment, please refer to the official [Cloudify](https://docs.cloudify.co/latest/) documentation.
* For the delete deployment operation, the deployment id that is already created and visible in your Cloudify Manager instance must be provided.

***Run Automation Dialog***

<table><tr><td>
  <img src="./images/how_to_run.png" alt="install" width="600px">
</td></tr></table>

***Cloudify Manager Console***

<table><tr><td>
  <img src="./images/cloudify_manager.png" alt="install" width="600px">
</td></tr></table>


## Test Environment
This pre-built was tested on a Cloudify Manager instance running on Itential's internal server and respective adapter instance setup on the Itential's  Automation platform. 
All testing was conducted using version 2020.2 of the Itential Automation Platform.

## Additional Information

Please use your Itential Customer Success account if you need support when using this Pre-Built.
